package mk.ukim.finki.izborzvanje.model;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import mk.ukim.finki.izborzvanje.model.Enum.CycleOfStudies;
import mk.ukim.finki.izborzvanje.model.Enum.TypeOfClass;

@Entity
@Data
@NoArgsConstructor
public class DetailsOfStudies{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Enumerated(EnumType.STRING)
    private CycleOfStudies cycleOfStudies;

    private String schoolYear;
    private String semester;
    private String subject;
    @Enumerated(EnumType.STRING)
    private TypeOfClass typeOfClass;
    private int numberOfClasses;
    private int students;
    private int consultations;

    @ManyToOne
    private User user;

}
