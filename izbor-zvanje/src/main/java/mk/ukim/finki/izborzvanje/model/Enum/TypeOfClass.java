package mk.ukim.finki.izborzvanje.model.Enum;

public enum TypeOfClass {
    LECTURE,
    AUDITORY,
    LABORATORY
}
