package mk.ukim.finki.izborzvanje.model;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import mk.ukim.finki.izborzvanje.model.Enum.Field;

import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class Criteria {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long Id; // 1
    private String name; // prv ciklus
    private double points; //2

//    @OneToMany(mappedBy = "criteria")
//    private NOCriteria noCriteria; //1 Sasho , 2 Ana

    @Enumerated(EnumType.STRING)
    private Field field; // NO, NI, SP

    public Criteria(String name, double points, Field field) {
        this.name = name;
        this.points = points;
        this.field = field;
    }

}
