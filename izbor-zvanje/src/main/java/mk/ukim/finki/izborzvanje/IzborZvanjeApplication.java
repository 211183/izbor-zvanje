package mk.ukim.finki.izborzvanje;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
public class IzborZvanjeApplication {

    public static void main(String[] args) {
        SpringApplication.run(IzborZvanjeApplication.class, args);
    }

}
