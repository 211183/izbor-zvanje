package mk.ukim.finki.izborzvanje.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Data;
import org.hibernate.annotations.CollectionIdJdbcTypeCode;

import java.util.List;

@Entity
@Table(name="app-user")
@Data
public class User {
    @Id
    private String username;
    private String Name;
    private String Surname;
    private String institution;
    private String fieldOfStudy;

    @OneToMany(mappedBy = "user")
    private List<NOCriteria> noCriteria;
}
