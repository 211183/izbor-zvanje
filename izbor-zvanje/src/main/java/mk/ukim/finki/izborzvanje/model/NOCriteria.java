package mk.ukim.finki.izborzvanje.model;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.autoconfigure.web.WebProperties;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

// ---------------------------------- GLAVNA TABELA NO --------------------------------------------
@Entity
@Data
@NoArgsConstructor
public class NOCriteria {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long Id;

    @ManyToOne
    private Criteria criteria;

   // @Query("SELECT SUM(ds.students*15*nc.criteria.points) FROM DetailsOfStudies ds, NOCriteria nc WHERE nc.criteria.id IN (1, 2, 3) GROUP BY ds.user, ds.cycleOfStudies")
    private double points;

    private double students;
    private String nameOfWorkTitle; // delo
    private String nameOfSubject;

    @ManyToOne
    private User user;

}
